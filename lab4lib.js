function convertNumberInMonth(number) {
	var mounthes = ["січня", "лютня", "березня", "квітня", "травня", "червня",
		"липня", "серпня", "вересня", "жовтня", "листопада", "грудня"];
	
	return mounthes[number];
	
}

function convertNumberInWeek(number) {
	var daysWeek = ["понеділок", "вівторок", "середа", "четверг", "пятниця", "субота", "неділя"];
	
	return daysWeek[number - 1];
}

function convertNumberInMonthRu(number) {
	var mounthes = ["января", "февраля", "марта", "апреля", "мая", "июня",
		"июля", "августа", "сентября", "октября", "ноября", "декабря"];
	
	return mounthes[number];
	
}

function convertNumberInWeekRu(number) {
	var daysWeek = ["понедельник", "вторник", "среда", "четверг", "пятница", "суббота", "воскресение"];
	
	return daysWeek[number - 1];
}

function convertNumberInMonthEn(number) {
	var mounthes = ["january", "february", "march", "april", "may", "june",
		"july", "august", "september", "october", "november", "december"];
	
	return mounthes[number];
	
}

function convertNumberInWeekEn(number) {
	var daysWeek = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];
	
	return daysWeek[number - 1];
}



function firstTask() {
	var date = new Date();
	
	var result = document.getElementById("first-result");
	result.innerHTML = "";
	
	var pDate = document.createElement('p'), 
		pDateDay = document.createElement('p'),
		pTime = document.createElement('p');
		
	pDate.innerText = "Дата: "+ date.getDate() + " " + convertNumberInMonth(date.getMonth()) +
	" " + date.getFullYear() + " року";
	
	pDateDay.innerText = "День: " + convertNumberInWeek(date.getDay());
	
	pTime.innerText = "Час: " + date.toLocaleTimeString();
	
	result.appendChild(pDate);
	result.appendChild(pDateDay);
	result.append(pTime);
	
	
}

function secondTask(date) {
	var pNumberDay = document.createElement('p'),
		pNameDay = document.createElement('p');
		
	var result = document.getElementById('second-result');
	result.innerHTML = "";

	var day = date.getDay();
	pNumberDay.innerText = "Номер дня: " + day;
	pNameDay.innerText = "Назва дня: " + convertNumberInWeek(day);
	
	result.appendChild(pNumberDay);
	result.appendChild(pNameDay);
	
	return {
		dayNumber: day,
		dayName: convertNumberInWeek(day)
	};
}

function thirdTask(value) {
	var result = document.getElementById('third-result');
	
	var currentDateInMillis = Date.now();
	var startDayInMillis = Date.parse(new Date(1970, 0, +value + 1, 0, 0, 0, 0));
	var dateResult = new Date(currentDateInMillis - startDayInMillis);
	
	result.innerText ="Дата: " + dateResult.toLocaleDateString();
}

function fourthTask() {
	var year = document.getElementById("year").value,
		numberMonth = document.getElementById("month").value;
	--numberMonth;
	var nextYear;
	if(numberMonth == 12) {
		nextYear = ++year;
	}
	
	var date = new Date(+year, +numberMonth, 2, 0, 0, 0, 0);
	var dateNextMonth = new Date(nextYear ? nextYear : +year, nextYear ? 1 : (+numberMonth + 1), 1, 0, 0, 0 ,0);
	
	var days = new Date(Date.parse(dateNextMonth) - Date.parse(date)).getDate();
	
	var result = document.getElementById("fourth-result");
	
	result.innerText = days;
}

function fifthTask() {
	var date = new Date();
	var beginToday = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);
	
	var milisDate = Date.parse(date);
	var milisBeginToday = Date.parse(beginToday);
	
	var seconds = Date.parse(new Date(milisDate - milisBeginToday)) / 1000;
	var secondsLeft = 86400 - seconds;
	
	var result = document.getElementById("fifth-result");
	
	result.innerText = "Прошло - " + seconds + ", осталось - " + secondsLeft;
}

function sixthTask() {
	var date = new Date();
	var result = document.getElementById("sixth-result");
	
	result.innerText = date.toLocaleDateString();
}

function seventhTask() {
	var firstDate = document.getElementById("first-date").value,
		secondDate = document.getElementById("second-date").value;
		
	var date = new Date(Date.parse(firstDate) - Date.parse(secondDate));
	
	var dateResult = Date.parse(date) / 3600000 / 24;
	
	var result = document.getElementById("seventh-result");
	
	result.innerText = "Різниця у днях: " + dateResult;
}

function eighthTask(date) {
	var thenDate = Date.parse(date);
	var result = document.getElementById("eighth-result");
	result.innerText = "тільки що";
	
	setInterval(function() {
		var nowDate = Date.now();
		
		if ((nowDate - thenDate) <= 60000) {
			result.innerText = parseInt((nowDate - thenDate) / 1000) + " сек. назад";
		} else if((nowDate - thenDate) <= 3600000) {
			result.innerText = parseInt((nowDate - thenDate) / 60000) + " хв. назад";
		} else {
			result.innerText = date.toLocaleString();
		}
		
	}, 1000);
}

function ninthTask(value) {
	var result = document.getElementById("ninth-result");
	
	var date = new Date(value);
	
	if(date.toString() == "Invalid Date") {
		result.innerText="Bad";
	} else {
		result.innerText="Good";
	}
}

function tenthTask(date) {
	var result = document.getElementById("tenth-result");
	var input = document.getElementById("tenth-len").value;
	switch(input) {
		case "ru":
			result.innerText = convertNumberInWeekRu(date.getDay()) + ", " + date.getDate() + 
			" " + convertNumberInMonthRu(date.getMonth()) + " нашей эры, " + date.toLocaleTimeString();
			break;
		case "ua":
			result.innerText = convertNumberInWeek(date.getDay()) + ", " + date.getDate() + 
			" " + convertNumberInMonth(date.getMonth()) + " нашої ери, " + date.toLocaleTimeString();
			break;
		case "en":
			result.innerText = convertNumberInWeekEn(date.getDay()) + ", " + date.getDate() + 
			" " + convertNumberInMonthEn(date.getMonth()) + " of our aera, " + date.toLocaleTimeString();
			break;
	}
}

(function addTask() {
	var input = document.getElementById("input");
	var date = new Date();
	input.value = date.toLocaleDateString(); 
	var arrMonth31 = [0, 2, 4, 6, 7, 9, 11];
	var arrMonth30 = [3, 5, 8, 10];
	
	window.addEventListener('keyup', function(e) {
		var codeKey = e.keyCode;
		
		if (codeKey == 38) {
			var year = date.getYear() + 1900;
			
			var ostacha = year % 4;
			
			if(ostacha > 2) {
				date.setYear(year + 4 - ostacha);
			} else {
				date.setYear(year - ostacha);
			}
			
		} else if(codeKey == 40) {
			var year = date.getYear() + 1900;
			
			date.setYear(year - 1);
		} else if(codeKey == 39) {
			var month = date.getMonth();
			
			if(month == 11) {
				date.setYear(1900 + date.getYear() + 1);
				month = 0;
				date.setMonth(month);
				input.value = date.toLocaleDateString(); 
				return;
			} 
			
			var fix = false;
			var elem = arrMonth31.find(function(val) {
				if(fix) {
					return true;
				}
				
				if(month == val || (month - val == 1 && month != 7)) {
					fix = true;
				}
				return false;
			});
			
			date.setMonth(elem);
		} else if(codeKey == 37) {
			var month = date.getMonth();
			
			if(month >= 10 || month <= 2) {
				date.setYear(1900 + date.getYear() + 1);
				month = 3;
				date.setMonth(month);
				input.value = date.toLocaleDateString(); 
				return;
			} 
			
			var fix = false;
			var elem = arrMonth30.find(function(val) {
				if(fix) {
					return true;
				}
				
				if(month == val || month - val == 1) {
					fix = true;
				}
				return false;
			});
			
			date.setMonth(elem);
		}
		
		
		input.value = date.toLocaleDateString(); 
	});
})();